<?php

namespace app\index\controller;

use app\exception\BusinessStatus;
use app\exception\BusinessException;
use app\utils\Result;

class Index {
    public function index() {
        echo "Hello,Thinkphp5.1";
    }

    public function exception() {
        //业务逻辑
        throw new BusinessException(BusinessStatus::NOT_LOGIN, ["key" => "val",]);
    }

    public function result() {
        //业务逻辑
        return Result::ok();
    }
}
