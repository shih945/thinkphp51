<?php
/**
 * 业务异常
 * @author SHIH
 * @date 2020/10/14 9:14
 */

namespace app\exception;


use Exception;

class BusinessException extends Exception {

    /**
     * 异常数据信息
     * @var object|string
     */
    private $errData;

    /**
     * BusinessException constructor.
     * @param array  $businessStatus 异常状态及提示
     * @param object $data 异常数据信息
     */
    public function __construct($businessStatus, $data) {
        $this->errData = $data ? $data : "";
        parent::__construct($businessStatus[1], $businessStatus[0]);
    }

    /**
     * 获取异常数据
     * @return object|string
     */
    public function getErrData() {
        return $this->errData;
    }
}