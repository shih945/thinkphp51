<?php
/**
 * 业务异常状态
 * @author SHIH
 * @date 2020/10/14 10:14
 */

namespace app\exception;


class BusinessStatus {

    /**
     * 成功，一切正常
     */
    const SUCCESS = [0, "success"];

    /**
     * 未登录或登录已过期,请重新登陆
     */
    const NOT_LOGIN = [401, "未登录或登录已过期,请重新登陆"];

    /**
     * 服务器错误
     */
    const UNKNOW_ERROR = [500, "服务器未知错误"];
}